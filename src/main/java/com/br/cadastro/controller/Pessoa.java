/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.cadastro.controller;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Douglas Freire
 */
@Entity
@Table(name = "PESSOA")
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "IDADE")
    private BigInteger idade;
    @Column(name = "SEXO")
    private String sexo;
    @Id
    @Column(name = "CPF")
    private String cpf;
    @Column(name = "ID_ENDERECO")
    private BigInteger idEndereco;
    @Column(name = "NUMERO_CONTA")
    private BigInteger numeroConta;

    public Pessoa() {
    }

    public Pessoa(String cpf) {
        this.cpf = cpf;
    }

    public Pessoa(String cpf, String nome, BigInteger idade, String sexo) {
        this.cpf = cpf;
        this.nome = nome;
        this.idade = idade;
        this.sexo = sexo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigInteger getIdade() {
        return idade;
    }

    public void setIdade(BigInteger idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public BigInteger getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(BigInteger idEndereco) {
        this.idEndereco = idEndereco;
    }

    public BigInteger getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(BigInteger numeroConta) {
        this.numeroConta = numeroConta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpf != null ? cpf.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.cpf == null && other.cpf != null) || (this.cpf != null && !this.cpf.equals(other.cpf))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.br.cadastro.controller.Pessoa[ cpf=" + cpf + " ]";
    }
    
}
